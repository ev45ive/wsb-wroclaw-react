```js

function updateState(state) {
    React.render(Component(state), elem)
}

function Component(state) {

    return {
        type: 'div',
        props: {
            onClick() {
                updateState({ ...state, newState: 123 })
            },
            children: SubComponent(state.subState)
        }
    }
}

