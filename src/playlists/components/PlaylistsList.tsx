import { Playlist } from '../../model/Playlist'
const clx = (...classes: (string | boolean)[]) => classes.filter(Boolean).join(' ')

interface Props {
    playlists: Playlist[]
    selectedId?: Playlist['id'],
    onSelect: (id: string) => void
}


const PlaylistsList = ({ playlists, selectedId, onSelect }: Props) => {

    return (
        <div>
            <div className="list-group">

                {playlists.map((item, index) =>
                    <button
                        className={clx("list-group-item list-group-item-action", item.id === selectedId && "active")}
                        onClick={() => onSelect(item.id)}
                        key={item.id}>
                        {index + 1}. {item.name}
                    </button>
                )}
            </div>
        </div>
    )
}

export default PlaylistsList
