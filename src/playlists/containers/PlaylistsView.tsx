import React, { Reducer, useEffect, useReducer, useState } from 'react'
import PlaylistsDetails from '../components/PlaylistsDetails'
import PlaylistsForm from '../components/PlaylistsForm'
import PlaylistsList from '../components/PlaylistsList'
import { Playlist } from '../../model/Playlist'
import { useHistory, useParams } from 'react-router'

interface Props { }

const playlistsData: Playlist[] = [{
    id: '123',
    name: 'Playlist 123',
    public: true,
    description: 'Best 123!!! <3'
}, {
    id: '234',
    name: 'Playlist 234',
    public: false,
    description: 'Best 234!!! <3'
}, {
    id: '345',
    name: 'Playlist 345',
    public: true,
    description: 'Best 345!!! <3'
}]

interface State {
    mode: 'edit' | 'details' | 'create'
    playlists: Playlist[],
    selectedId?: Playlist['id']
    selectedPlaylist?: Playlist
}
const initialState: State = {
    mode: 'details',
    playlists: playlistsData,
}

type SELECT_PLAYLIST = {
    type: 'SELECT_PLAYLIST'
    payload: { id: Playlist['id'] }
}
type UPDATE_PLAYLIST = {
    type: 'UPDATE_PLAYLIST'
    payload: { draft: Playlist }
}
type ADD_PLAYLIST = {
    type: 'ADD_PLAYLIST'
    payload: { draft: Playlist }
}
type SWITCH_MODE = {
    type: 'SWITCH_MODE'
    payload: { mode: State['mode'] }
}
type ENABLE_CREATE_MODE = { type: 'ENABLE_CREATE_MODE' }

const selectPlaylist = (id: Playlist['id']): SELECT_PLAYLIST => ({ type: 'SELECT_PLAYLIST', payload: { id } })
const switchMode = (mode: State['mode']): SWITCH_MODE => ({ type: 'SWITCH_MODE', payload: { mode } })
const enableCreateMode = (): ENABLE_CREATE_MODE => ({ type: 'ENABLE_CREATE_MODE' })
const updatePlaylist = (draft: Playlist): UPDATE_PLAYLIST => ({ type: 'UPDATE_PLAYLIST', payload: { draft } })
const createPlaylist = (draft: Playlist): ADD_PLAYLIST => ({ type: 'ADD_PLAYLIST', payload: { draft } })

type Actions =
    | SELECT_PLAYLIST
    | SWITCH_MODE
    | ENABLE_CREATE_MODE
    | UPDATE_PLAYLIST
    | ADD_PLAYLIST

const reducer: Reducer<State, Actions> = (state, action) => {
    console.log(action)

    switch (action.type) {
        case 'SELECT_PLAYLIST': return {
            ...state,
            selectedId: action.payload.id,
            selectedPlaylist: state.playlists.find(p => p.id === action.payload.id)
        }
        case 'SWITCH_MODE': return { ...state, mode: action.payload.mode }
        case 'ENABLE_CREATE_MODE': return {
            ...state, mode: 'create',
            selectedPlaylist: { id: '', name: '', description: '', public: false }
        }
        case 'UPDATE_PLAYLIST': return {
            ...state,
            mode: 'details',
            playlists: state.playlists.map(p => p.id === action.payload.draft.id ? action.payload.draft : p),
            selectedId: action.payload.draft.id,
            selectedPlaylist: action.payload.draft
        }
        case 'ADD_PLAYLIST': return {
            ...state,
            mode: 'details',
            playlists: [...state.playlists, action.payload.draft],
            selectedId: action.payload.draft.id,
            selectedPlaylist: action.payload.draft
        }
        default:
            return state
    }
}



const PlaylistsView = (props: Props) => {
    const [{
        mode,
        playlists,
        selectedId,
        selectedPlaylist
    }, dispatch] = useReducer(reducer, initialState)

    const { push } = useHistory()
    const { playlist_id } = useParams<{ playlist_id: string }>()

    useEffect(() => {
        dispatch(selectPlaylist(playlist_id))
    }, [playlist_id])

    const savePlaylist = (draft: Playlist) => {
        dispatch(updatePlaylist(draft))
    }

    const addPlaylist = (draft: Playlist) => {
        draft.id = (Date.now() + Math.ceil(Math.random() * 100_000)).toString()
        dispatch(createPlaylist(draft))
    }

    return (
        <div>
            {/* .row>.col*2 */}
            <div className="row">
                <div className="col">
                    <PlaylistsList
                        selectedId={selectedId}
                        playlists={playlists}
                        onSelect={id => {
                            // dispatch(selectPlaylist(id))
                            push('/playlists/' + id)
                        }} />

                    <button className="btn btn-info btn-block float-end mt-3"
                        onClick={() => dispatch(enableCreateMode())}>
                        Create new playlist
                    </button>
                </div>
                <div className="col">

                    {selectedPlaylist && mode === 'details' && <PlaylistsDetails
                        playlist={selectedPlaylist}
                        onEdit={() => dispatch(switchMode('edit'))} />}

                    {selectedPlaylist && mode === 'edit' && <PlaylistsForm
                        playlist={selectedPlaylist}
                        onClickCancel={() => dispatch(switchMode('details'))}
                        onSave={savePlaylist} />}

                    {selectedPlaylist && mode === 'create' && <PlaylistsForm
                        playlist={selectedPlaylist}
                        onClickCancel={() => dispatch(switchMode('details'))}
                        onSave={addPlaylist} />}

                    {!selectedPlaylist && <p>Please select playlist</p>}

                </div>
            </div>
        </div>
    )
}

export default PlaylistsView
