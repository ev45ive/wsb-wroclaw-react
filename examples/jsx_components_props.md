```tsx

window.React = React
window.ReactDOM = ReactDOM
interface User {
  name: string;
  pet: string;
}

// root.innerHTML = `<div>
//     <p>Ala ma <b id="animal">papugę</b> i <input id="inp"></p>
// </div>`

const user: User = { name: 'Alice', pet: 'Żyrafę' }

// function UserCard(props: { user: User }) {
//   /// ...
//   /// ...
//   return <li id="123" className="test" >
//     <p>{props.user.name} ma {props.user.pet} i <input type="text" /> </p>
//   </li>
// }

// const UserCard = (props: { user: User }) => <li id="123" className="test" >
//   <p>{props.user.name} ma {props.user.pet} i <input type="text" /> </p>
// </li>

// const UserCard = (props: { user: User }) => {
//   // const a = props.a;
//   // const b = props.b;
//   // const c = props.c;
//   // const { a, b, c } = props;

//   const user = props.user
//   const { user } = props;

//   return <li id="123" className="test" >
//     <p>{user.name} ma {user.pet} i <input type="text" /> </p>
//   </li>
// }

// const UserCard = ({ user }: { user: User, children: React.ReactNode }) => <li id="123" className="test" > 
//    <p>{children} ma {user.pet} i <input type="text" /> </p>
// </li>

const UserCard: React.FC<{ user: User }> = ({
  user,
  children
}) => <li id="123" className="test" >
    <p>{children} ma {user.pet} i <input type="text" /> </p>
  </li>


function update(state: { users: User[] }) {

  ReactDOM.render(<ul>
    {state.users.map(item => <UserCard user={item} key={item.name}> <b>{item.name}</b> </UserCard>)}
  </ul>, document.getElementById('root'))
}

let state = {
  placki: 123,
  users: [
    { name: 'Alice', pet: 'Cat' },
    { name: 'Bob', pet: 'Dog' },
    { name: 'Kate', pet: 'Fish' },
  ]
}
setInterval(() => {
  state = {
    ...state, users: [...state.users.slice(2), ...state.users.slice(0, 2)]
  }
  update(state)
}, 1000)
```