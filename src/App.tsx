import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import 'bootstrap/dist/css/bootstrap.css'
import React, { useLayoutEffect } from 'react';
import { OAuthCallback, useOAuth2Token } from 'react-oauth2-hook';
import { NavLink, Redirect, Route, Switch } from 'react-router-dom';
import MusicSearchView from './music-search/containers/MusicSearchView';
import PlaylistsView from './playlists/containers/PlaylistsView';

function App() {
  const [token, getToken, setToken] = useOAuth2Token({
    authorizeUrl: 'https://accounts.spotify.com/authorize',
    clientID: '8d71338a511f4e8b825cbd856ac071fb',
    redirectUri: 'http://localhost:3000/',
    scope: [],
  })

  const login = () => {
    getToken()
  }

  useLayoutEffect(() => {
    if (!token) { /* getToken() */ return }

    const handle = axios.interceptors.request.use((config: AxiosRequestConfig) => {
      config.headers['Authorization'] = 'Bearer ' + token;
      return config;
    })

    return () => axios.interceptors.request.eject(handle)
  }, [token])

  useLayoutEffect(() => {
    axios.interceptors.response.use(resp => resp, error => {
      if (error.message == 'Cancelled') { return }

      if (!axios.isAxiosError(error)) {
        throw new Error('Unknown error')
      }
      // Token expired!
      if (error.response?.status === 401) {
        getToken()
      }

      if (isSpotifyError(error)) {
        return Promise.reject(error.response?.data.error || new Error('Network error'))
      }
      throw new Error(error.message)
    })
  }, [])


  if (window.location.hash.includes('access_token')) {
    return <div>
      <h3>Logging you in...</h3>
      <OAuthCallback />
    </div>
  }


  return (
    <div className="App">
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div className="container">
          <NavLink className="navbar-brand" to="/" exact={true}>MusicApp</NavLink>

          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">

              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/search">Search</NavLink>
              </li>

            </ul><span className="navbar-text ms-auto">
              <a href="#" onClick={login}>Login</a>
            </span>
          </div>
        </div>
      </nav>

      <div className="container">
        <div className="row">
          <div className="col">

            <Switch>
              <Redirect path="/" exact={true} to="/playlists" />
              <Route path="/playlists/:playlist_id" exact={true} component={PlaylistsView} />
              <Route path="/playlists" exact={true} component={PlaylistsView} />
              <Route path="/search" component={MusicSearchView} />
              {/* <Route path="**" component={PageNotFoundComponent} /> */}
              <Redirect path="**" to="/search" /> {/* <- must be last! */}
            </Switch>

          </div>
        </div>
      </div>
    </div>
  );
}

export default App;

type SpotifyErrorResponse = AxiosError<{ error: { message: string } }>

function isSpotifyError(error: any): error is SpotifyErrorResponse {
  return Boolean(error.response?.data?.error?.message)
}
