import React, { FormEvent, useEffect, useState } from 'react'
import { Playlist } from '../../model/Playlist'

interface Props {
    playlist: Playlist,
    // onCancel: () => void
    onClickCancel(): void,
    onSave: (draft: Playlist) => void
}


const PlaylistsForm = ({ playlist, onClickCancel, onSave }: Props) => {
    const [playlistName, setPlaylistName] = useState(playlist.name)
    const [isPublic, setIsPublic] = useState(playlist.public)
    const [description, setDescription] = useState(playlist.description)

    useEffect(() => {
        setPlaylistName(playlist.name)
        setIsPublic(playlist.public)
        setDescription(playlist.description)
    }, [playlist])

    const submitForm = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()

        // if(playlistName.length>3){ /* error */}
        const draft = {
            ...playlist,
            name: playlistName,
            public: isPublic,
            description
        }
        onSave(draft)
    }

    return (
        <form onSubmit={submitForm}>
            <pre>{JSON.stringify(playlist, null, 2)}</pre>

            <pre>{JSON.stringify({
                name: playlistName,
                public: isPublic,
                description
            }, null, 2)}</pre>

            <div className="form-group mb-3" >
                <label htmlFor="playlist_name">Name:</label>

                <input type="text" className="form-control" id="playlist_name" placeholder="Name"
                    value={playlistName}
                    onChange={event => setPlaylistName(event.currentTarget.value)}
                />

                <small id="helpId" className="form-text text-muted">
                    {playlistName.length} / 170
                </small>
            </div >

            <div className="form-check mb-3">
                <label className="form-check-label">
                    <input type="checkbox" className="form-check-input" checked={isPublic} onChange={e => setIsPublic(e.target.checked)} /> Public
              </label>
            </div>

            <div className="form-group mb-3">
                <label htmlFor="playlist_description">Description:</label>
                <textarea className="form-control" id="playlist_description" rows={3} value={description} onChange={e => setDescription(e.target.value)} />
            </div>

            <button className="btn btn-danger" type="button" onClick={onClickCancel}>Cancel</button>
            <button className="btn btn-success" type="submit">Save</button>
        </form>
    )
}

export default PlaylistsForm