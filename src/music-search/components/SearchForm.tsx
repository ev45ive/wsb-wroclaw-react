import React, { useEffect, useRef, useState } from 'react'

interface Props {
    query: string
    onSearch(query: string): void
}

const SearchForm = ({ onSearch, query: parentQuery }: Props) => {
    const [query, setQuery] = useState(parentQuery)
    const queryInputRef = useRef<HTMLInputElement | null>(null)
    // const isFirst = useRef(true)

    useEffect(() => { setQuery(parentQuery) }, [parentQuery])

    useEffect(() => {
        // if (isFirst.current) { isFirst.current = false; return }
        if (parentQuery === query) { return } // Avoid double search

        const handle = setTimeout(() => {
            onSearch(query)
        }, 500)

        return () => clearTimeout(handle)
    }, [query])

    useEffect(() => {
        // document.querySelector('#queryInput')?.focus() // Avoid Global Refs
        queryInputRef.current?.focus() // Use local refs!
    }, [])

    return (
        <div className="mb-3">
            <div className="input-group">

                <input type="text" className="form-control" placeholder="Search" ref={queryInputRef}
                    value={query} onChange={e => setQuery(e.currentTarget.value)} />

                {/* <input type="text" className="form-control" placeholder="Search" ref={(elem) => console.log(elem)}
                    value={query} onChange={e => setQuery(e.currentTarget.value)} /> */}

                <button className="btn btn-outline-secondary"
                    onClick={() => onSearch(query)}>
                    Search
                </button>
            </div>
        </div>
    )
}

export default SearchForm
