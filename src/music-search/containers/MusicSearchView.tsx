import React, { useEffect, useRef, useState } from 'react'
import { Album, SearchResponse, SimpleAlbum, validateSearchResponse } from '../../model/Search'
import SearchForm from '../components/SearchForm'
import SearchResults from '../components/SearchResults'
import axios, { AxiosError, AxiosResponse } from 'axios'
import { useHistory, useLocation } from 'react-router'

interface Props { }


const MusicSearchView = (props: Props) => {
    const [query, setQuery] = useState('')
    const [results, setResults] = useState<Album[]>([])
    const [message, setMessage] = useState('')

    const { push, replace } = useHistory()
    const { search } = useLocation()
    const queryParam = new URLSearchParams(search).get('q')

    useEffect(() => {
        if (queryParam) { setQuery(queryParam) }
    }, [queryParam])

    useEffect(() => {
        setMessage('')
        setResults([])
        if (!query) { return }

        const handle = axios.CancelToken.source()
        axios
            .get('https://api.spotify.com/v1/search', {
                params: {
                    type: 'album',
                    q: query
                },
                cancelToken: handle.token
            })
            .then(validateSearchResponse)
            .then(response => setResults(response.data.albums.items))
            .catch((error: any) => setMessage(error.message))

        return () => handle.cancel('Cancelled')
    }, [query])

    const submitSearch = (query: string) => {
        push('/search?q=' + query)
    }

    return (
        <div>

            <div className="row">
                <div className="col">
                    <SearchForm onSearch={submitSearch} query={query} />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    {message && <p className="alert alert-danger">{message}</p>}

                    <SearchResults results={results} />
                </div>
            </div>

        </div>
    )
}

export default MusicSearchView
