```ts

function takeFirst<T>(arr: Array<T>): T {
    return arr[0]
}
const res1 = takeFirst<number>([1, 2, 3])
const res2 = takeFirst(['1', '2', '3'])
const res3 = takeFirst(['1', 2, null, true])
// res3.toString()

function handleCases(value: string | number | boolean) {
    if ('number' === typeof value) {
        return value
    } else if ('string' === typeof value) {
        return value
    } else if ('boolean' === typeof value) {
        return value
    } else {
        // const _never: never = res3 // Cannot assign null to never
        exhaustivenessCheck(value)
        // const x = 1; // Unreachable code detected.
    }
}

function exhaustivenessCheck(never: never): never {
    throw new Error('Unexpected option ' + never)
}


